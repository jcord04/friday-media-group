(function ($, Drupal) {

  /*Drupal.behaviors.friday = {
    attach: function(context, settings) {
      // Get your Yeti started.
    }
  };*/

  function carouselSimple() {
    $('.homepage--carousel--simple').bxSlider({
      mode: 'fade',
      auto: true,
      width: '100%',
      speed: 900,
      pause: 5000,
      pager: false,
      controls: false
    });
  }


$(document).ready(function() {
  carouselSimple();
});

})(jQuery, Drupal);



