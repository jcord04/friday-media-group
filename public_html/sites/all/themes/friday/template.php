<?php

/**
 * Implements template_preprocess_html().
 *
 */
function friday_preprocess_html(&$variables) {

  // Add Google fonts
  drupal_add_css('http://fonts.googleapis.com/css?family=Open+Sans:400italic,700,300,600,400', array('type' => 'external'));

  // Add conditional CSS for IE. To use uncomment below and add IE css file
  //drupal_add_css(path_to_theme() . '/css/ie.css', array('weight' => CSS_THEME, 'browsers' => array('!IE' => FALSE), 'preprocess' => FALSE));

  // Need legacy support for IE downgrade to Foundation 2 or use JS file below
  // drupal_add_js('http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE7.js', 'external');
}


/**
 * Implements theme_field().
 *
 */
function friday_field($variables) {

  // Leave standard Drupal 7 code, we will then overite when needed.
  $output = '';

  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<div class="field-label"' . $variables['title_attributes'] . '>' . $variables['label'] . ':&nbsp;</div>';
  }

  // Render the items.
  $output .= '<div class="field-items"' . $variables['content_attributes'] . '>';
  foreach ($variables['items'] as $delta => $item) {
    $classes = 'field-item ' . ($delta % 2 ? 'odd' : 'even');
    $output .= '<div class="' . $classes . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</div>';
  }
  $output .= '</div>';

  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . '"' . $variables['attributes'] . '>' . $output . '</div>';

  // Switch and preform field changes depending on the field machine name
  switch ($variables['element']['#field_name']) {
    case 'field_hoempage_carousel_image':
      $output = '<div class="homepage--carousel columns"><ul class="homepage--carousel--simple">';

      // Render the items.
      foreach ($variables['items'] as $delta => $item) {
        $output .= '<li>' . drupal_render($item) . '</li>';
      }

      $output .= '</ul></div>';
    break;

    case 'field_homepage_logo_collection':
      $output = '<div class="homepage--logos"><ul class="row">';

      // Render the items.
      foreach ($variables['items'] as $delta => $item) {
        $output .= '<li class="small-3 large-6 column">' . drupal_render($item) . '</li>';
      }

      $output .= '</ul></div>';
    break;

    case 'title':
      $output = '<div class="columns">';

      // Render the items.
      foreach ($variables['items'] as $delta => $item) {
        $output .= drupal_render($item);
      }

      $output .= '</div>';
    break;

    case 'field_homepage_body':
      $output = '<section class="wysiwyg columns">';

      // Render the items.
      foreach ($variables['items'] as $delta => $item) {
        $output .= drupal_render($item);
      }

      $output .= '</section>';
    break;

  }
  return $output;
}


/**
 * Implements template_preprocess_page
 *
 */
function friday_preprocess_page(&$variables) {

  // Create a new Footer nenu variable for easy output.
  $variables['fmg_footer_menu'] = '<ul class="footer--user-links"><li>&copy; ' . date('Y') . ' ' . t('All rights reserved') .'</li>';

  $menu = menu_tree('menu-footer-menu');
  $rendered_menu = drupal_render($menu);

  // Remove all tags apart from li and A as we want to add this manually
  $rendered_menu = strip_tags($rendered_menu,"<li>, <a>");

  $variables['fmg_footer_menu'] .= $rendered_menu;
  $variables['fmg_footer_menu'] .= '</ul>';
}


/**
 * Implements theme_menu_link
 *
 */
function friday_menu_link(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';

  switch ($element['#theme']) {
    case 'menu_link__menu_footer_menu':
        // Footer menu
        if ($element['#below']) {
          $sub_menu = drupal_render($element['#below']);
        }
        $output = l($element['#title'], $element['#href'], $element['#localized_options']);
        return '<li>| ' . $output . $sub_menu . "</li>\n";
      break;

    default:
        //Drupal standard
        if ($element['#below']) {
          $sub_menu = drupal_render($element['#below']);
        }
        $output = l($element['#title'], $element['#href'], $element['#localized_options']);
        return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
      break;
  }
}

/**
 * Implements template_preprocess_node
 *
 */
//function STARTER_preprocess_node(&$variables) {
//}

/**
 * Implements hook_preprocess_block()
 */
//function STARTER_preprocess_block(&$variables) {
//  // Add wrapping div with global class to all block content sections.
//  $variables['content_attributes_array']['class'][] = 'block-content';
//
//  // Convenience variable for classes based on block ID
//  $block_id = $variables['block']->module . '-' . $variables['block']->delta;
//
//  // Add classes based on a specific block
//  switch ($block_id) {
//    // System Navigation block
//    case 'system-navigation':
//      // Custom class for entire block
//      $variables['classes_array'][] = 'system-nav';
//      // Custom class for block title
//      $variables['title_attributes_array']['class'][] = 'system-nav-title';
//      // Wrapping div with custom class for block content
//      $variables['content_attributes_array']['class'] = 'system-nav-content';
//      break;
//
//    // User Login block
//    case 'user-login':
//      // Hide title
//      $variables['title_attributes_array']['class'][] = 'element-invisible';
//      break;
//
//    // Example of adding Foundation classes
//    case 'block-foo': // Target the block ID
//      // Set grid column or mobile classes or anything else you want.
//      $variables['classes_array'][] = 'six columns';
//      break;
//  }
//
//  // Add template suggestions for blocks from specific modules.
//  switch($variables['elements']['#block']->module) {
//    case 'menu':
//      $variables['theme_hook_suggestions'][] = 'block__nav';
//    break;
//  }
//}

//function STARTER_preprocess_views_view(&$variables) {
//}

/**
 * Implements template_preprocess_panels_pane().
 *
 */
//function STARTER_preprocess_panels_pane(&$variables) {
//}

/**
 * Implements template_preprocess_views_views_fields().
 *
 */
//function STARTER_preprocess_views_view_fields(&$variables) {
//}

/**
 * Implements theme_form_element_label()
 * Use foundation tooltips
 */
//function STARTER_form_element_label($variables) {
//  if (!empty($variables['element']['#title'])) {
//    $variables['element']['#title'] = '<span class="secondary label">' . $variables['element']['#title'] . '</span>';
//  }
//  if (!empty($variables['element']['#description'])) {
//    $variables['element']['#description'] = ' <span data-tooltip="top" class="has-tip tip-top" data-width="250" title="' . $variables['element']['#description'] . '">' . t('More information?') . '</span>';
//  }
//  return theme_form_element_label($variables);
//}

/**
 * Implements hook_preprocess_button().
 */
//function STARTER_preprocess_button(&$variables) {
//  $variables['element']['#attributes']['class'][] = 'button';
//  if (isset($variables['element']['#parents'][0]) && $variables['element']['#parents'][0] == 'submit') {
//    $variables['element']['#attributes']['class'][] = 'secondary';
//  }
//}

/**
 * Implements hook_form_alter()
 * Example of using foundation sexy buttons
 */
//function STARTER_form_alter(&$form, &$form_state, $form_id) {
//  // Sexy submit buttons
//  if (!empty($form['actions']) && !empty($form['actions']['submit'])) {
//    $classes = (is_array($form['actions']['submit']['#attributes']['class']))
//      ? $form['actions']['submit']['#attributes']['class']
//      : array();
//    $classes = array_merge($classes, array('secondary', 'button', 'radius'));
//    $form['actions']['submit']['#attributes']['class'] = $classes;
//  }
//}

/**
 * Implements hook_form_FORM_ID_alter()
 * Example of using foundation sexy buttons on comment form
 */
//function STARTER_form_comment_form_alter(&$form, &$form_state) {
  // Sexy preview buttons
//  $classes = (is_array($form['actions']['preview']['#attributes']['class']))
//    ? $form['actions']['preview']['#attributes']['class']
//    : array();
//  $classes = array_merge($classes, array('secondary', 'button', 'radius'));
//  $form['actions']['preview']['#attributes']['class'] = $classes;
//}


/**
 * Implements template_preprocess_panels_pane().
 */
// function zurb_foundation_preprocess_panels_pane(&$variables) {
// }

/**
* Implements template_preprocess_views_views_fields().
*/
/* Delete me to enable
function THEMENAME_preprocess_views_view_fields(&$variables) {
 if ($variables['view']->name == 'nodequeue_1') {

   // Check if we have both an image and a summary
   if (isset($variables['fields']['field_image'])) {

     // If a combined field has been created, unset it and just show image
     if (isset($variables['fields']['nothing'])) {
       unset($variables['fields']['nothing']);
     }

   } elseif (isset($variables['fields']['title'])) {
     unset ($variables['fields']['title']);
   }

   // Always unset the separate summary if set
   if (isset($variables['fields']['field_summary'])) {
     unset($variables['fields']['field_summary']);
   }
 }
}

// */

/**
 * Implements hook_css_alter().
 */
//function STARTER_css_alter(&$css) {
//  // Always remove base theme CSS.
//  $theme_path = drupal_get_path('theme', 'zurb_foundation');
//
//  foreach($css as $path => $values) {
//    if(strpos($path, $theme_path) === 0) {
//      unset($css[$path]);
//    }
//  }
//}

/**
 * Implements hook_js_alter().
 */
//function STARTER_js_alter(&$js) {
//  // Always remove base theme JS.
//  $theme_path = drupal_get_path('theme', 'zurb_foundation');
//
//  foreach($js as $path => $values) {
//    if(strpos($path, $theme_path) === 0) {
//      unset($js[$path]);
//    }
//  }
//}
